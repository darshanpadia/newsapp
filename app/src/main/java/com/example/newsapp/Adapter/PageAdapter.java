package com.example.newsapp.Adapter;

import android.os.Bundle;

import com.example.newsapp.Fragments.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PageAdapter extends FragmentPagerAdapter {

    private int totalTabs;

    public PageAdapter(@NonNull FragmentManager fm, int totalTabs) {
        super(fm);
        this.totalTabs = totalTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        if (position == 1) {
            Bundle bundle = new Bundle();
            bundle.putString("source", "bbc-news");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;

        }else if (position == 2){
            Bundle bundle = new Bundle();
            bundle.putString("source", "google-news-in");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;

        }else if (position == 3){
            Bundle bundle = new Bundle();
            bundle.putString("source", "infobae");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;
        }
        else if (position == 4){
            Bundle bundle = new Bundle();
            bundle.putString("source", "msnbc");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;

        }
        else if (position == 5){
            Bundle bundle = new Bundle();
            bundle.putString("source", "mtv-news");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;
        }
        else if (position == 6){
            Bundle bundle = new Bundle();
            bundle.putString("source", "nbc-news");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;
        }
        else if (position == 7){
            Bundle bundle = new Bundle();
            bundle.putString("source", "news24");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;
        }
        else if (position == 8){
            Bundle bundle = new Bundle();
            bundle.putString("source", "talksport");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;
        }
        else if (position == 9){
            Bundle bundle = new Bundle();
            bundle.putString("source", "techcrunch");

            Fragment fragobj = new fragment();
            fragobj.setArguments(bundle);
            return fragobj;
        }
        Bundle bundle = new Bundle();
        bundle.putString("source", "fox-news");

        Fragment fragobj = new fragment();
        fragobj.setArguments(bundle);
        return fragobj;
    }

    @Override
    public int getCount() {
        return totalTabs;
    }
}
