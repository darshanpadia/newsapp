package com.example.newsapp.Adapter;


import android.content.Context;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.RelativeLayout;


import com.example.newsapp.Models.Article;
import com.example.newsapp.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.recyclerview.widget.RecyclerView;

public class ArticleAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private ArrayList<Article> articleList;
    private Context context;
    private int lPosition;
    private RelativeLayout container;

    public ArticleAdapter(ArrayList<Article> articleList, RelativeLayout container) {
        this.articleList = articleList;
        this.container = container;

    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_article, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, final int position) {



        final Article article = articleList.get(position);

        String published = article.getPublishedAt();
        List<String> sList = Arrays.asList(published.split("T"));
        String date = sList.get(0);
        String time = sList.get(1);

        Log.d("debug", sList.toString());

        Picasso.get().load(article.getUrlToImage()).into(holder.imageView);

        holder.title.setText(article.getTitle());
        holder.desc.setText(article.getDescription());
        holder.date.setText(article.getDate());
        holder.time.setText(article.getTime());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToArticlePage(article.getUrl());
            }
        });

        holder.desc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToArticlePage(article.getUrl());
            }
        });
        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redirectToArticlePage(article.getUrl());
            }
        });


    }

    private void redirectToArticlePage(String url) {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(context, Uri.parse(url));
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

}
