package com.example.newsapp.Adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.newsapp.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ItemViewHolder extends RecyclerView.ViewHolder{


    public ImageView imageView;
    public TextView title,desc,time,date;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.thumbnail);
        title = itemView.findViewById(R.id.title);
        desc = itemView.findViewById(R.id.desc);
        time = itemView.findViewById(R.id.time);
        date = itemView.findViewById(R.id.date);


    }
}
