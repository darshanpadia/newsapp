package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.alibaba.fastjson.JSON;

import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.newsapp.Adapter.PageAdapter;
import com.example.newsapp.Models.Article;
import com.example.newsapp.Utils.ApiClient;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Article> articleList = new ArrayList<>();
    private String responseString;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        ApiClient apiClient = new ApiClient();
        articleList = apiClient.getArticles("bbc-news");


        MaterialToolbar toolbar = findViewById(R.id.toolbarMainActivity);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        setSupportActionBar(toolbar);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.fox_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.bbc_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.google_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.infobae)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.msnbc)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.mtv_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.nbc_news)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.news24)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.talksport)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.techcrunch)));

        PagerAdapter adapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


//        RequestQueue mRequestQueue = Volley.newRequestQueue(MainActivity.this);







    }

}

