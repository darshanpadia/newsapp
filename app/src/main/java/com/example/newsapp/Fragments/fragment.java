package com.example.newsapp.Fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsapp.Adapter.ArticleAdapter;
import com.example.newsapp.Models.Article;
import com.example.newsapp.R;
import com.example.newsapp.Utils.ApiClient;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class fragment extends Fragment {

    private ArrayList<Article> articleList = new ArrayList<>();
    private Handler handler = new Handler();
    private RecyclerView recyclerView;
    private RelativeLayout container;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment,container,false);
        String source = getArguments().getString("source");
        ApiClient apiClient = new ApiClient();
        articleList = apiClient.getArticles(source);
        Log.d("debug", articleList.get(2).getTitle());

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerViewImage);
        container = view.findViewById(R.id.container);
        
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1));
        
        getArticles();

    }

    private void getArticles() {

        new Thread(new Runnable() {
            @Override
            public void run() {

                ArticleAdapter articleAdapter = new ArticleAdapter(articleList,container);
                articleAdapter.notifyDataSetChanged();
                recyclerView.setAdapter(articleAdapter);

                }
        }).start();
    }
}
