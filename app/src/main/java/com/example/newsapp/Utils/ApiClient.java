package com.example.newsapp.Utils;

import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.example.newsapp.Models.Article;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ApiClient {

    private String BASE_URL = "https://newsapi.org/v2/top-headlines?";
    private String API_KEY = "apiKey=5ed7514dfb95417fa0ee00b1fbedcdef";

    private ArrayList<Article> articleList = new ArrayList<>();

    public  ArrayList<Article> getArticles(String source) {
        OkHttpClient httpClient = new OkHttpClient();
        String url = BASE_URL + "sources=" + source + "&" + API_KEY;
//        Log.d("debug", url);

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = null;
        try {
            JsonParser parser = new JsonParser();
            response = httpClient.newCall(request).execute();
            String responseString = response.body().string();
            JsonObject jsonObject = parser.parse(responseString).getAsJsonObject();
//            Log.e("debug", responseString);
            JsonArray jsonArray = jsonObject.get("articles").getAsJsonArray();
//            Log.d("debug", jsonArray.toString());
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject jo = jsonArray.get(i).getAsJsonObject();
//                Log.d("debug", String.valueOf(jo));
                Article article = JSON.parseObject(String.valueOf(jo), Article.class);
                articleList.add(article);
            }
        } catch (IOException e) {
//            Log.e("debug", e.toString());
        }
        return articleList;
    }

}
